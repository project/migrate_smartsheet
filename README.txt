Migrate Smartsheet

Below is an example of the *fields* key in my migration file.

fields:
    -
      name: row_id
      label: 'ID'
      selector: id
    -
      name: cell_array
      label: 'Cell Array'
      selector: cells
  ids:
    row_id:
      type: string

Below is a very simple example of populating the title field.

title:
    plugin: smartsheet
    source: cell_array
    columnID: YOUR_COLUMN_ID
    return_key: 'value'

