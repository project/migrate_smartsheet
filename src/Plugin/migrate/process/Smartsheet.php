<?php

namespace Drupal\migrate_smartsheet\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
* The data returned from the Smartsheet API provides
* two arrays, 'Columns' and 'Rows'. This plugin will allow you
* to grab cell data based on the ColumnID (columns[0][id]) within
* each row.
*
* @MigrateProcessPlugin(
*   id = "smartsheet"
* )
*
* Simple example usage:
*
* @code
* field_text:
*   plugin: smartsheet
*   source: array
*   column_id: int
*   return_key: 'string'
* @endcode
*
*
* There is also the option to set up a comparison.
* Provide the value you want to compare in the key 'compare_value'.
* If this is set, then the 'return_true_value' and 'return_false_value'
* keys are required.
*
* @code
* field_text:
*   plugin: smartsheet
*   source: array
*   column_id: int
*   return_key: 'string'
*   compare_value: (optional)
*   return_true_value: required if compare_value is set
*   return_false_value: required if compare_value is set
* @endcode
*
*/

class Smartsheet extends ProcessPluginBase {

/**
* {@inheritdoc}
*/
public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

  $search = $this->configuration['column_id'];
  $return_key = $this->configuration['return_key'];
  (!empty($this->configuration['compare_value']) ? $compare_value = $this->configuration['compare_value'] : $compare_value = NULL);

  if ($search) {
    if (is_array($value) || $value instanceof \Traversable) {
      if($value['columnId'] == $search){
        if($compare_value !== NULL){
          if($value[$return_key] !== $this->configuration['compare_value']){
            if(!empty($this->configuration['return_false_value'])) return $this->configuration['return_false_value']];
          }
          else{
            if(!empty($this->configuration['return_true_value'])) return $this->configuration['return_true_value'];
          }
        }
        else{
          if(!empty($value[$return_key])) return $value[$return_key];
        }
      }
    }
    else {
      throw new MigrateException(sprintf('%s is not traversable', var_export($value, TRUE)));
    }
  }
  else {
    throw new MigrateException(sprintf('search value is not set'));
  }
}

}
